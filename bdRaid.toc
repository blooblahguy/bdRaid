## Interface: 80000
## Title: |cffA02C2Fbd|rRaid
## DefaultState: Enabled
## LoadOnDemand: 0
## Notes: Raid timers and utilties addon thats focused towards automation and cutting edge progression.
## Author: Blooblahguy

lib\LibStub.lua
lib\CallbackHandler-1.0.lua
lib\LibSharedMedia-3.0.lua

init.lua
functions.lua
core.lua

modules/maiden.lua
modules/kj.lua
modules/atb.lua